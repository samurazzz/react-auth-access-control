// = Libs =
import { FC } from 'react';

// = Styles =
import classes from './page.module.scss';

// = Components =
import Header from '@/containers/header/header';
import Footer from '@/containers/footer/footer';
import Container from '@/containers/container/container';

const Page: FC = (props) => {
	const { children } = props;

	return (
		<>
			<Header/>

			<main className={classes['main']}>
				<Container>
					{children}
				</Container>
			</main>

			<Footer/>
		</>
	);
};

export default Page;
