// = Libs =
import { FC } from 'react';
import { Link } from 'react-router-dom';

// = Styles =
import classes from './header.module.scss';

// = Components =
import Container from '@/containers/container/container';

const Header: FC = () => {
	return (
		<header className={classes['header']}>
			<Container>
				<div className={classes['header__content']}>

					<div className={classes['header__logo']}>
						<span>RAAC</span>
						<Link to='/' />
					</div>

					<div className={classes['header__auth']}>
						<Link to='/sign-in' className={classes['header__sign-in']}>
							Sign in
						</Link>
						<Link to='sign-up' className={classes['header__sign-up']}>
							Sign up
						</Link>
					</div>

				</div>
			</Container>
		</header>
	);
};

export default Header;
