// = Libs =
import { FC } from 'react';

// = Styles =
import classes from './footer.module.scss';

// = Components =
import Container from '@/containers/container/container';

const Footer: FC = () => {
	const current_year = new Date().getFullYear();

	return (
		<footer className={classes['footer']}>
			<Container>
				<div className={classes['footer__content']}>
					<span className={classes['footer__copyright']}>
						© {current_year} All rights reserved.
					</span>
				</div>
			</Container>
		</footer>
	);
};

export default Footer;
