// = Libs =
import { FC } from 'react';

// = Styles =
import classes from './container.module.scss';

const Container: FC = (props) => {
	const { children } = props;

	return (
		<div className={classes['container']}>
			{children}
		</div>
	);
};

export default Container;
