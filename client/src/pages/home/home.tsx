// = Libs =
import { FC } from 'react';

// = Components =
import Page from '@/containers/page/page';

const Home: FC = () => {
	return (
		<Page>
			Homepage
		</Page>
	);
};

export default Home;
