// = Libs =
import * as Yup from 'yup';

// = Types =
import { SignInFormModel } from './sign-in.types';

// = Constants =
import VALIDATION from '@/constants/validation';

const INITIAL_VALUES: SignInFormModel = {
	email: '',
	password: '',
};

const VALIDATION_SCHEMA = Yup.object().shape({
	email: VALIDATION.required_email,
	password: VALIDATION.required_string,
});

export { INITIAL_VALUES, VALIDATION_SCHEMA };
