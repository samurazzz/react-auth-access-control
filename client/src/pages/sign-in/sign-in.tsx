// = Libs =
import { FC } from 'react';
import { Formik, Form } from 'formik';

// = Styles =
import classes from './sign-in.module.scss';

// = Types =
import { SignInFormModel } from './sign-in.types';

// = Constants =
import { INITIAL_VALUES, VALIDATION_SCHEMA } from './sign-in.constants';

// = Components =
import Page from '@/containers/page/page';
import Button from '@/components/ui/button/button';
import FormGroup from '@/components/core/form/components/form-group';
import FormInput from '@/components/core/form/components/form-input';

const SignIn: FC = () => {
	const handleSubmit = (values: SignInFormModel) => {
		console.log(values);
	}

	return (
		<Page>
			<section className={classes['sign-in']}>
				<h1 className={classes['sign-in__title']}>
					Sign in
				</h1>
				<Formik
					initialValues={INITIAL_VALUES}
					validationSchema={VALIDATION_SCHEMA}
					onSubmit={handleSubmit}
					validateOnChange={false}
				>
					<Form className={classes['sign-in__form']}>
						<FormGroup>
							<FormInput
								name="email"
								placeholder="Email"
								fullWidth
							/>
							<FormInput
								name="password"
								type="password"
								placeholder="Password"
								fullWidth
							/>
						</FormGroup>

						<Button type="submit" fullWidth>
							Sign in
						</Button>
					</Form>
				</Formik>
			</section>
		</Page>
	);
};

export default SignIn;
