interface SignInFormModel {
	email: string;
	password: string;
}

export type { SignInFormModel };
