// = Libs =
import { FC } from 'react';
import { Formik, Form } from 'formik';

// = Styles =
import classes from './sign-up.module.scss';

// = Types =
import { SignUpFormModel } from './sign-up.types';

// = Constants =
import { INITIAL_VALUES, VALIDATION_SCHEMA } from './sign-up.constants';

// = Components =
import Page from '@/containers/page/page';
import Button from '@/components/ui/button/button';
import FormGroup from '@/components/core/form/components/form-group';
import FormInput from '@/components/core/form/components/form-input';

const SignUp: FC = () => {
	const handleSubmit = (values: SignUpFormModel) => {
		console.log(values);
	}

	return (
		<Page>
			<section className={classes['sign-up']}>
				<h1 className={classes['sign-up__title']}>
					Sign up
				</h1>
				<Formik
					initialValues={INITIAL_VALUES}
					validationSchema={VALIDATION_SCHEMA}
					onSubmit={handleSubmit}
					validateOnChange={false}
				>
					<Form className={classes['sign-up__form']}>
						<FormGroup>
							<FormInput
								name="name"
								placeholder="Name"
								fullWidth
							/>
							<FormInput
								name="email"
								placeholder="Email"
								fullWidth
							/>
							<FormInput
								name="password"
								type="password"
								placeholder="Password"
								fullWidth
							/>
							<FormInput
								name="confirmPassword"
								type="password"
								placeholder="Confirm password"
								fullWidth
							/>
						</FormGroup>

						<Button type="submit" fullWidth>
							Sign up
						</Button>
					</Form>
				</Formik>
			</section>
		</Page>
	);
};

export default SignUp;
