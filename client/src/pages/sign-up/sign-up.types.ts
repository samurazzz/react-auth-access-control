interface SignUpFormModel {
	name: string;
	email: string;
	password: string;
	confirmPassword: string;
}

export type { SignUpFormModel };
