// = Libs =
import * as Yup from 'yup';

// = Types =
import { SignUpFormModel } from './sign-up.types';

// = Constants =
import VALIDATION from '@/constants/validation';

const INITIAL_VALUES: SignUpFormModel = {
	name: '',
	email: '',
	password: '',
	confirmPassword: '',
};

const VALIDATION_SCHEMA = Yup.object().shape({
	name: VALIDATION.required_string,
	email: VALIDATION.required_email,
	password: VALIDATION.required_password,
	confirmPassword: VALIDATION.match_password,
});

export { INITIAL_VALUES, VALIDATION_SCHEMA };
