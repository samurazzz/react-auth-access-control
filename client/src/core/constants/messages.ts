const MESSAGES = {
	FORM: {
		required: 'Данное поле обязательно',
		not_valid: 'Введите все данные в поле',
		min_password: 'Пароль должен содержать минимум 8 символов',
		match_password: 'Пароли должны совпадать',
	}
};

export default MESSAGES;
