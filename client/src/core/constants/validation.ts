// = Libs =
import * as Yup from 'yup';

// = Constants =
import MESSAGES from '@/constants/messages';

const VALIDATION = {
	required_string: Yup.string().required(MESSAGES.FORM.required),
	required_email: Yup.string()
		.email(MESSAGES.FORM.not_valid)
		.required(MESSAGES.FORM.required),
	required_password: Yup.string()
		.required(MESSAGES.FORM.required)
		.min(8, MESSAGES.FORM.min_password),
	match_password: Yup.string()
		.oneOf([Yup.ref('password'), null], MESSAGES.FORM.match_password),
};

export default VALIDATION;
