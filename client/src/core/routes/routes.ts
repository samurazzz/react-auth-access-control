// = Libs =
import { RouteProps } from 'react-router-dom';

// = Pages =
import Home from '@/pages/home/home';
import SignIn from '@/pages/sign-in/sign-in';
import SignUp from '@/pages/sign-up/sign-up';

interface Route extends RouteProps {
	key: string;
}

const routes: Route[] = [
	{
		path: '/',
		key: 'HOME',
		component: Home,
		exact: true,
	},
	{
		path: '/sign-in',
		key: 'SIGN_IN',
		component: SignIn,
	},
	{
		path: '/sign-up',
		key: 'SIGN_UP',
		component: SignUp,
	},
];

export default routes;
