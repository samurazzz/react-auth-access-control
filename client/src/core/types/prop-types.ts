namespace PropTypes {
	export type Color = 'primary' | 'secondary' | 'success' | 'warning' | 'danger';
}

export default PropTypes;
