// = Libs =
import { FC } from 'react';
import clsx from 'clsx';

// = Styles =
import classes from './input.module.scss';

// = Types =
import { InputModel } from './input.types';

const Input: FC<InputModel> = (props) => {
	const {
		type = 'text',
		placeholder = '',
		value = '',
		name = '',
		className = '',
		fullWidth = false,
		onChange = () => {}
	} = props;

	return (
		<input
			type={type}
			value={value}
			name={name}
			className={clsx({
				[classes['input']]: true,
				[classes['input--full-width']]: fullWidth,
			}, className)}
			placeholder={placeholder}
			onChange={onChange}
		/>
	);
};

export default Input;
