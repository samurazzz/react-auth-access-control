// = Types =
import { SyntheticEvent } from 'react';

type InputType = 'text' | 'email' | 'password' | 'number';

interface InputModel {
	value: string;
	type?: InputType;
	name?: string;
	placeholder?: string;
	className?: string;
	fullWidth?: boolean;
	onChange: (e: SyntheticEvent) => void;
}

export type { InputModel, InputType }
