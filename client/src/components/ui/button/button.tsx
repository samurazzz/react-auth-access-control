// = Libs =
import { FC } from 'react';
import clsx from 'clsx';

// = Styles =
import classes from './button.module.scss';

// = Types =
import { ButtonModel } from './button.types';

const Button: FC<ButtonModel> = (props) => {
	const {
		children,
		type = 'button',
		color = 'primary',
		fullWidth = false,
		disabled = false,
		className = '',
	} = props;

	return (
		<button
			type={type}
			disabled={disabled}
			className={clsx({
				[classes['button']]: true,
				[classes[`button--${color}`]]: color,
				[classes['button--full-width']]: fullWidth,
			}, className)}
		>
			{children}
		</button>
	);
};

export default Button;
