// = Types =
import PropTypes from '@/types/prop-types';

type ButtonType = 'button' | 'submit' | 'reset';

interface ButtonModel {
	type: ButtonType;
	color?: PropTypes.Color;
	fullWidth?: boolean;
	disabled?: boolean;
	className?: string;
}

export type { ButtonModel, ButtonType };
