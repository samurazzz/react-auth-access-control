// = Libs =
import { FC } from 'react';

// = Styles =
import classes from '../form.module.scss';

const FormGroup: FC = (props) => {
	const { children } = props;

	return (
		<div className={classes['form__group']}>
			{children}
		</div>
	);
};

export default FormGroup;
