// = Libs =
import { FC } from 'react';
import { Field, FieldProps } from 'formik';

// = Styles =
import classes from '../form.module.scss';

// = Types =
import { FormInputModel } from '../form.types';

// = Components =
import Input from '@/components/ui/input/input';
import FormError from '@/components/core/form/components/form-error';

const FormInput: FC<FormInputModel> = (props) => {
	const { name } = props;

	return (
		<Field name={name}>
			{(params: FieldProps) => {
				const { field, meta } = params;

				return (
					<>
						<Input className={classes['form__field']} {...props} {...field} />
						{(meta.touched && meta.error) && <FormError text={meta.error} />}
					</>
				);
			}}
		</Field>
	);
};

export default FormInput;
