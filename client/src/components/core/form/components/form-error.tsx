// = Libs =
import { FC } from 'react';

// = Styles =
import classes from '../form.module.scss';

// = Types =
import { FormErrorModel } from '../form.types';

const FormError: FC<FormErrorModel> = (props) => {
	const { text = '' } = props;

	return <span className={classes.form__error}>{text}</span>;
};

export default FormError;
