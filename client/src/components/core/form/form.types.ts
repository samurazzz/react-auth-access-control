import { InputModel } from '@/components/ui/input/input.types';

interface FormInputModel extends Partial<InputModel> {
	name: string;
}

interface FormErrorModel {
	text: string;
}

export type {
	FormInputModel,
	FormErrorModel,
};
