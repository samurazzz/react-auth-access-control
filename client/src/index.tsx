// = Libs =
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';

// = Imports =
import '@/styles/main.scss';
import App from '@/app/app';

const container = document.getElementById('root');

ReactDOM.render(
	<Router>
		<App/>
	</Router>,
	container
);
