// = Libs =
import { FC } from 'react';
import { Switch, Route } from 'react-router-dom';

// = Imports =
import routes from '@/routes/routes';

const App: FC = () => {
	return (
		<Switch>
			{routes.map((route) => (
				<Route {...route} />
			))}
		</Switch>
	);
};

export default App;
